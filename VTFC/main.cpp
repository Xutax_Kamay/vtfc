#include <iostream>
#include <vector>
#include <fstream>

int main( size_t argc , char **argv )
{
    if ( argc < 2 )
    {
        std::cout << "Please drop atleast one image (png or jpg) to convert into vtf.\n";
        return 0;
    }

    for ( size_t i = 1; i < argc; ++i )
    {
        std::ifstream file;
        file.open( argv[i] , std::ios_base::in );
        if ( !file.is_open() || !file.good() || file.fail() )
        {
            std::cout << "Can't open " << argv[i] << '\n';
            continue;
        }

        std::cout << "Converting " << argv[i] << '\n';
        char header[4];
        file.read( header , 4 );
        file.seekg( 0 );

        if ( header[0] == -1 && header[1] == -40 && header[2] == -1 && header[3] == -32 )
        {
            std::cout << "File is jpg!\n";
        }

        if ( header[0] == -119 && header[1] == 80 && header[2] == 78 && header[3] == 71 )
        {
            std::cout << "File is png!\n";
        }

        file.close();
    }

    return 0;
}